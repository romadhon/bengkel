package id.co.sevima.bengkel.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import id.co.sevima.bengkel.MapsActivity;
import id.co.sevima.bengkel.R;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created by Sevima on 8/10/2018.
 */

public class MainActivity extends AppCompatActivity {
    private FusedLocationProviderClient client;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        client = LocationServices.getFusedLocationProviderClient(MainActivity.this);
        Button button = (Button) findViewById(R.id.btn_location);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkPermission();
                if (ActivityCompat.checkSelfPermission(MainActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                client.getLastLocation().addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                            intent.putExtra("lon",location.getLongitude());
                            intent.putExtra("lat",location.getLatitude());
                            startActivity(intent);
                        }
                    }
                });


            }
        });
    }
    public void checkPermission(){
        ActivityCompat.requestPermissions(this,new String[]{ACCESS_FINE_LOCATION},1);
    }
}
