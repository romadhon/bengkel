package id.co.sevima.bengkel;

import android.app.FragmentManager;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ahmadrosid.lib.drawroutemap.DrawMarker;
import com.ahmadrosid.lib.drawroutemap.DrawRouteMaps;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import id.co.sevima.bengkel.fragment.DialogOrder;
import id.co.sevima.bengkel.maps.GetNearbyPlacesData;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    private ArrayList<LatLng> latlngs = new ArrayList<>();
    private MarkerOptions options = new MarkerOptions();


    public static final int REQUEST_LOCATION_CODE = 99;
    int PROXIMITY_RADIUS = 10000;
    double latitude,longitude;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapsActivity.this);
        longitude = getIntent().getExtras().getDouble("lon");
        latitude = getIntent().getExtras().getDouble("lat");
        final Object dataTransfer[] = new Object[2];
        final GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData();
        Button hospital = (Button) findViewById(R.id.btn_hospital);
        hospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.clear();
                String hospital = "hospital";
                url = getUrl(latitude, longitude, hospital);
                dataTransfer[0] = mMap;
                dataTransfer[1] = url;

                getNearbyPlacesData.execute(dataTransfer);
                Toast.makeText(MapsActivity.this, "Showing Nearby Hospitals", Toast.LENGTH_SHORT).show();
            }
        });
        Button restaurant = (Button) findViewById(R.id.btn_restaurant);
        restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.clear();
                String resturant = "bengkel";
                url = getUrl(latitude, longitude, resturant);
                dataTransfer[0] = mMap;
                dataTransfer[1] = url;

                getNearbyPlacesData.execute(dataTransfer);
                Toast.makeText(MapsActivity.this, "Showing Nearby Restaurants", Toast.LENGTH_SHORT).show();
            }
        });
        Button school = (Button) findViewById(R.id.btn_school);
        school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.clear();
                String school = "school";
                url = getUrl(latitude, longitude, school);
                dataTransfer[0] = mMap;
                dataTransfer[1] = url;

                getNearbyPlacesData.execute(dataTransfer);
                Toast.makeText(MapsActivity.this, "Showing Nearby Schools", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        System.out.println("INTENT : "+getIntent().getExtras().getDouble("lat"));
        System.out.println("INTENT 2 : "+getIntent().getExtras().getDouble("lon"));
        latlngs.add(new LatLng(getIntent().getExtras().getDouble("lat"),getIntent().getExtras().getDouble("lon")));
        latlngs.add(new LatLng(-7.28981,112.7777954));
        latlngs.add(new LatLng(-7.2880858,112.778547));
        latlngs.add(new LatLng(-7.287106,112.7795773));

        for (int i=0;i<latlngs.size();i++){
            if (i==0){
                options.position(latlngs.get(0));
                options.title("Lokasi Kamu");
                options.snippet("someDesc");
                options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_motor));
                mMap.addMarker(options);
            }else{
                options.position(latlngs.get(i));
                options.title("Yamaha Yes");
                options.snippet("someDesc");
                options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_bengkel));
                mMap.addMarker(options);
            }


        }

        checkPermission();
        if (ActivityCompat.checkSelfPermission(MapsActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapsActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        DrawRouteMaps.getInstance(this)
                .draw(latlngs.get(0), latlngs.get(1), mMap);
        DrawMarker.getInstance(this).draw(mMap, latlngs.get(0), R.drawable.marker_motor, "Origin Location");
        DrawMarker.getInstance(this).draw(mMap, latlngs.get(1), R.drawable.marker_bengkel, "Destination Location");

        Location startPoint=new Location("locationA");
        startPoint.setLatitude(getIntent().getExtras().getDouble("lat"));
        startPoint.setLongitude(getIntent().getExtras().getDouble("lon"));

        Location endPoint=new Location("locationA");
        endPoint.setLatitude(-7.28981);
        endPoint.setLongitude(112.7777954);

        final double distance=startPoint.distanceTo(endPoint);

        System.out.println("JARAK : "+distance);
        mMap.setMyLocationEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlngs.get(0), 12.0f));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (!marker.getTitle().equals("Lokasi Kamu")){
                    showDialog(distance,distance*3);
                }
                return false;
            }
        });



    }

    public void checkPermission(){
        ActivityCompat.requestPermissions(this,new String[]{ACCESS_FINE_LOCATION},1);
    }

    private String getUrl(double latitude , double longitude , String nearbyPlace)
    {

        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location="+latitude+","+longitude);
        googlePlaceUrl.append("&radius="+PROXIMITY_RADIUS);
        googlePlaceUrl.append("&type="+nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key="+"AIzaSyBLEPBRfw7sMb73Mr88L91Jqh3tuE4mKsE");

        Log.d("MapsActivity", "url = "+googlePlaceUrl.toString());

        return googlePlaceUrl.toString();
    }


    void showDialog(double distance, double cost) {

        FragmentManager manager = getFragmentManager();
        android.app.Fragment frag = manager.findFragmentByTag("order");
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        DialogOrder order = new DialogOrder();
        Bundle bundle = new Bundle();
        bundle.putDouble("jarak", distance);
        bundle.putDouble("biaya", cost);
        order.setArguments(bundle);
        order.show(manager, "order");
    }

}
