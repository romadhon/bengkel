package id.co.sevima.bengkel.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import id.co.sevima.bengkel.R;

/**
 * Created by Sevima on 8/11/2018.
 */

public class DialogOrder extends DialogFragment{
    Dialog dg;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dg = new Dialog(getActivity(), R.style.FullscreenAppCompatMenuRightTheme);
        dg.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        dg.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dg.setContentView(R.layout.fragment_dialog);
        DecimalFormat df2 = new DecimalFormat(".##");

        double km = getArguments().getDouble("jarak")/1000;
        TextView jarak = (TextView) dg.findViewById(R.id.txt_distance);
        jarak.setText("Jarak : "+ df2.format(km)+" Km");
        TextView biaya = (TextView) dg.findViewById(R.id.txt_cost);
        biaya.setText("Biaya Transportasi : Rp."+df2.format(getArguments().getDouble("biaya")));
        FrameLayout free_zone = (FrameLayout) dg.findViewById(R.id.free_zone);
        free_zone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dg.dismiss();
            }
        });

        Button telpon = (Button) dg.findViewById(R.id.btn_telpon);
        telpon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        Button bantuan = (Button) dg.findViewById(R.id.btn_bantuan);
        bantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        return dg;
    }

}
